// Avoid `console` errors in browsers that lack a console.
(function () {
    var method;
    var noop = function () {
    };
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );

// IE 10 == Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)


$(document).ready(function () {
    jQuery(function ($) {
        $('input[type=tel]').mask("+(38)(099) 999-99-99");
    });

    $('.submit-btn').click(function () {
        var formID = ('#' + this.form.id);
        $(formID).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3,
                    maxlength: 16
                },
                comment: {
                    required: true
                },

                phone: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {

                name: {
                    required: "Это поле обязательно для заполнения",
                    minlength: "Должно быть минимум 3 символа",
                    maxlength: "Максимальное число символов - 16"
                },

                phone: {
                    required: "Это поле обязательно для заполнения"
                },

                comment: {
                    required: "Это поле обязательно для заполнения"
                },

                email: {
                    required: "Это поле обязательно для заполнения",
                    email: "Введите правильный E-Mail"
                }

            },

            submitHandler: function (form) {
                var request;
                var serializedData;
                var inputs;
                var callbackModalID;

                serializedData = $(form).serialize();
                inputs = $(formID).find('input, select, button, textarea');
                callbackModalID = '#callbackModal';

                request = $.ajax({
                    url: "mail.php",
                    type: "post",
                    data: serializedData
                });
                request.done(function () {
                    $(formID).trigger("reset");
                    $(callbackModalID).modal('show');
                    var parent = $(formID).closest('.modal');
                    var modalID = ('#' + parent.attr("id"));
                    if ($(modalID).hasClass('in')) {
                        $(modalID).modal('hide');
                        return false;
                    } else {
                        return false;
                    }
                });

                request.fail(function (jqXHR, textStatus, errorThrown) {
                    console.error(
                        "The following error occured: " + textStatus, errorThrown);
                });

                request.always(function () {
                    inputs.prop("disabled", true);
                });
            }
        });
    });

    // DEFEND  map iframe
    $('#overlay').on('mouseup', function() {
        $('#map').addClass('scrolloff');
    });

    $('#overlay').on('mousedown', function() {
        $('#map').removeClass('scrolloff');
    });

    $('#map').mouseleave(function () {
        $('#map').addClass('scrolloff');
    });
});
